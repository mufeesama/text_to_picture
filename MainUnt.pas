unit MainUnt;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, Vcl.Menus, Vcl.StdCtrls, cxButtons,
  cxTextEdit, Vcl.ExtCtrls, cxLabel, cxMaskEdit, cxDropDownEdit, cxFontNameComboBox, cxGroupBox,
  cxCheckBox, cxRadioGroup, cxSpinEdit, Vcl.ExtDlgs, dxCore, dxColorEdit, Vcl.Imaging.pngimage;

type
  TMainFrm = class(TForm)
    edtMain: TcxTextEdit;
    pnlMain: TPanel;
    cxGroupBox1: TcxGroupBox;
    Panel2: TPanel;
    cbbfont: TcxFontNameComboBox;
    lblpreview: TcxLabel;
    btnPreview: TcxButton;
    btnsave: TcxButton;
    pnlsave: TPanel;
    cxRadioGroup1: TcxRadioGroup;
    cxRadioGroup2: TcxRadioGroup;
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    cxSpinEdit1: TcxSpinEdit;
    chkbold: TcxCheckBox;
    chkstatic: TcxCheckBox;
    chkul: TcxCheckBox;
    chkso: TcxCheckBox;
    dxColorEdit1: TdxColorEdit;
    cxRadioGroup3: TcxRadioGroup;
    Panel6: TPanel;
    Panel7: TPanel;
    dxColorEdit2: TdxColorEdit;
    pm1: TPopupMenu;
    N1: TMenuItem;
    PNG1: TMenuItem;
    SaveDialog1: TSaveDialog;
    procedure btnPreviewClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure N1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainFrm: TMainFrm;

implementation

{$R *.dfm}

/// 半角转全角的函数
function ToSDB(AInput: WideString): WideString;
var
  I: Integer;
  nChar: Cardinal;
begin
  Result := AInput;
  for I := 1 to Length(AInput) do
  begin
    nChar := Cardinal(WideChar(AInput[I]));
    if nChar = 32 then
    begin
      Result[I] := WideChar(12288);
      Continue;
    end;
    if (nChar < 127) then
      Result[I] := WideChar(nChar + 65248)
  end;
end;


/// 全角转半角的函数
function ToDBC(const AInput: WideString): WideString;
var
  I: Integer;
  nChar: Cardinal;
begin
  Result := AInput;
  for I := 1 to Length(AInput) do
  begin
    nChar := Cardinal(WideChar(AInput[I]));
    if nChar = 12288 then
    begin
      Result[I] := WideChar(32);
      Continue;
    end;
    if (nChar > 65280) and (nChar < 65375) then
      Result[I] := WideChar(nChar - 65248)
  end;
end;

function ColorBetween(const ColorA, ColorB: TColor; const Percent: Integer): TColor;
var
  R1, G1, B1: Byte;
  R2, G2, B2: Byte;
begin
  R1 := GetRValue(ColorA);
  G1 := GetGValue(ColorA);
  B1 := GetBValue(ColorA);
  R2 := GetRValue(ColorB);
  G2 := GetGValue(ColorB);
  B2 := GetBValue(ColorB);

  Result := RGB(Percent * (R2 - R1) div 100 + R1, Percent * (G2 - G1) div 100 + G1, Percent * (B2 - B1) div 100 + B1);
end;

function WindowSnap(WndHandle: HWND; Destbmp: TBitmap): Boolean;
var
  r: TRect;
  user32DLLHandle: THandle;
  PrintWindowAPI: function(sourceHandle: HWND; destinationHandle: HDC; nFlags: UINT): BOOL; stdcall;
begin
  Result := False;
  user32DLLHandle := GetModuleHandle(user32);
  if user32DLLHandle <> 0 then
  begin
    @PrintWindowAPI := GetProcAddress(user32DLLHandle, 'PrintWindow');
    if @PrintWindowAPI <> nil then
    begin
      GetWindowRect(WndHandle, r);
      Destbmp.Width := r.Right - r.Left;
      Destbmp.Height := r.Bottom - r.Top;
      Destbmp.Canvas.Lock;
      try
        Result := PrintWindowAPI(WndHandle, Destbmp.Canvas.Handle, 0);
      finally
        Destbmp.Canvas.Unlock;
      end;
    end;
  end;
end;

procedure TMainFrm.btnPreviewClick(Sender: TObject);
var
  i, j, fs: Integer;
  vstr: string;
  edt: TcxLabel;
  pnl: TPanel;
begin
  for i := 0 to pnlMain.ControlCount - 1 do
  begin
    pnlMain.Controls[i].Free;
  end;
  vstr := Trim(edtMain.Text);
  fs := 110;
  with lblpreview.Style.Font do
  begin
    SIZE := fs;
    Name := cbbfont.FontName;
  end;

  pnl := TPanel.Create(Self);
  pnl.Color := clWhite;
  pnl.BorderStyle := bsNone;
  pnl.BevelOuter := bvNone;
  pnl.Parent := pnlMain;
  pnl.Height := lblpreview.Height;
  pnl.Width := 0;
  //pnl.Width := lblpreview.Width * 4;
  pnl.Top := 0;

  vstr := Trim(edtMain.Text);
  for j := Length(vstr) downto 1 do
  begin
    with lblpreview.Style.Font do
    begin
      SIZE := fs;
      Name := cbbfont.Name;
    end;
    pnl.Width := pnl.Width + lblpreview.Width + cxSpinEdit1.Value;
    edt := TcxLabel.Create(pnl);
    edt.AutoSize := False;
    //edt.Style.BorderStyle := ebsNone;
    with edt.Style.Font do
    begin
      Name := cbbfont.FontName;
      Charset := GB2312_CHARSET;
      SIZE := fs;
      if chkbold.Checked then
        Style := Style + [fsBold];
      if chkstatic.Checked then
        Style := Style + [fsItalic];
      if chkul.Checked then
        Style := Style + [fsUnderline];
      if chkso.Checked then
        Style := Style + [fsStrikeOut];
    end;
    edt.Parent := pnl;
    case cxRadioGroup1.EditValue of
      1:
        edt.Properties.Alignment.Vert := taTopJustify;
      2:
        edt.Properties.Alignment.Vert := taVCenter;
      3:
        edt.Properties.Alignment.Vert := taBottomJustify;
    end;
    case cxRadioGroup2.EditValue of
      1:
        edt.Properties.Alignment.Horz := taLeftJustify;
      2:
        edt.Properties.Alignment.Horz := taCenter;
      3:
        edt.Properties.Alignment.Horz := taRightJustify;
    end;

    case cxRadioGroup3.EditingValue of
      1:
        begin
          edt.Style.Font.Color := Random(16777216);
        end;
      2:
        begin
          edt.Style.Font.Color := ColorBetween(dxColorEdit1.ColorValue, dxColorEdit2.ColorValue,
            Round(100 / Length(vstr) * j));
        end;
    end;

    edt.Align := alLeft;
    edt.Anchors := [akTop, akRight, akBottom];
    with lblpreview.Style.Font do
    begin
      SIZE := fs;
      Name := cbbfont.Name;
    end;
    //edt.Height := pnl.Height;
    edt.Width := lblpreview.Width + cxSpinEdit1.Value;
    edt.Caption := vstr[j];
    edt.Transparent := True;
    edt.Properties.Transparent := True;
    Dec(fs, 10);
  end;

  pnlMain.Height := pnl.Height;
  pnlMain.Width := pnl.Width;
  pnlMain.Left := (pnlsave.Width - pnlMain.Width) div 2;
  pnlMain.top := (pnlsave.Height - pnlMain.Height) div 2;
end;

procedure TMainFrm.FormShow(Sender: TObject);
begin
  cbbfont.ItemIndex := cbbfont.Properties.Items.Count - 1;
end;

procedure TMainFrm.N1Click(Sender: TObject);
begin
  var bmp := TBitmap.Create;
  WindowSnap(pnlMain.Handle, bmp);
  if (Sender as TMenuItem).Tag = 0 then
  begin
    with SaveDialog1 do
    begin
      DefaultExt := 'bmp';
      Filter := 'Bitmaps (*.bmp)|*.bmp';
      FileName := edtMain.Text + '.bmp';
      if Execute then
        bmp.SaveToFile(FileName);
    end;
  end
  else
  begin
    var png := TPngImage.Create;
    bmp.PixelFormat := pf8bit;
    bmp.HandleType := bmDIB;
    png.AssignHandle(bmp.Handle, True, clWhite);
    png.TransparentColor := clWhite;
    png.Transparent := True;
    png.CompressionLevel := 9;
    with SaveDialog1 do
    begin
      DefaultExt := 'png';
      Filter := 'Png (*.png)|*.png';
      FileName := edtMain.Text + '.png';
      if Execute then
        png.SaveToFile(FileName);
    end;
    png.Free;
  end;
  bmp.Free;
end;

end.

